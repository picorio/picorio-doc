================
Pygmy_ES1Y Board
================

Pygmy_ES1Y chip Introduction
============================

.. figure:: ../images/board/board1.png
    :name: board1


.. figure:: ../images/board/board2.png
    :name: board2

Pygmy_ES1Y EVB Hardware
=======================

Pygmy_ES1Y EVB Hardware configuration
--------------------------------------

.. figure:: ../images/board/board3.png
    :name: board3

Pygmy_ES1Y EVB expansion port
-----------------------------

.. figure:: ../images/board/board4.png
    :name: board4

Expansion port1 J28 40pin GPIOs Define
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: ../images/board/board5.png
    :name: board5

Expansion port2 J29 40pin GPIOs Define
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: ../images/board/board6.png
    :name: board6

Pygmy_ES1Y share IO (multifunction)
===================================

.. figure:: ../images/board/board7.png
    :name: board7

How to use EVB?
===============
Pygmy_ES1Y EVB boot configuration
---------------------------------
.. figure:: ../images/board/board8.png
    :name: board8
.. figure:: ../images/board/board9.png
    :name: board9
Pygmy_ES1Y EVB interface switch
-------------------------------
.. figure:: ../images/board/board10.png
    :name: board10
Pygmy_ES1Y EVB debug
--------------------
.. figure:: ../images/board/board11.png
    :name: board11