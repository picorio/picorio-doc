===================================
Getting Started
===================================

Get the Source Code
-------------------

You can clone the source code of RRV64 along with its simulator using ``git``: ::

     $ git clone https://gitlab.com/picorio/rrv64.git


Prerequisites
-------------
Several tools are needed to build the project.

1. `Verilator <https://www.veripool.org/projects/verilator/wiki/Installing>`_ : SystemVerilog Translator and simulator
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On Ubuntu, executing the following command should suffice: ::

        $ sudo apt-get install verilator

For other OS, you can install Verilator with Git. See `here <https://www.veripool.org/projects/verilator/wiki/Installing/>`_ for more information.


2. `Gtkwave <http://gtkwave.sourceforge.net/>`_ : Wave viewer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To make use of Verilator waveform tracing, you will need to have GTKwave installed.

3. `RISC-V GNU Compiler Toolchain <https://github.com/riscv/riscv-gnu-toolchain>`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 * Choose Newlib for installation.
 * For RRV64, the configuration should be: ``./configure --prefix=/opt/riscv --with-arch=rv64gc --with-abi=lp64d``
 * To add ``$PATH`` into PATH,  If you choose, say, ``/opt/riscv`` as prefix: ::

        $ vim ~/.bashrc

   append ``export PATH=$PATH:/opt/riscv/bin`` into .bashrc file, then save & exit, then ::

        $ source ~/.bashrc

Compile & Run simulation
------------------------

With VCS
~~~~~~~~

To compile RRV64 with VCS ::

     $ cd rrv64/tb
     $ make vcs

And then it will be compiled by `VCS`, to run the simulation ::

     $ make vcs_run

The default program to be executed is Dhrystone

With Verilator
~~~~~~~~~~~~~~

`Verilator <https://github.com/verilator/verilator>`_ is an open-source simulator, it provides verilog/systemverilog compilation function similar to VCS.

 * Build RRV64 and run program with RRV64 in Verilator ::

        $ cd rrv64/tb
        $ make ver

   And then it will be compiled by `Verilator`, to run the simulation ::

          $ make ver_run

   The Dhrystone program is executed by default. You will see the execution result of Dhrystone in about one minute.


 * To change the program running in the RRV64 processor, edit the file ``rrv64/tb/rrv64/top.sv``, input the path to the binary file you want to execute.


Check the Waveform file
^^^^^^^^^^^^^^^^^^^^^^^

With the argument ``+trace`` after ``./Vtestbench``,
the program will produce a waveform file with suffix ``.vcd`` in the folder ``logs``
under its corresponding folder prefixed with ``sim_``.

To check the waveform file, we use `Gtkwave <http://gtkwave.sourceforge.net/>`_,
say the ``.vcd`` file named ``vlt_dump.vcd``: ::

     $ gtkwave vlt_dump.vcd
