Cache overview
==============

So far, the RRV64 core is equipped with private L1 instruction & data cache and unified L2 cache, the coherent L1 data cache is in progress.

The overall design of our internal memory hierarchy is illustrated in following blockdiagram.

Single core
-----------
   .. figure:: ../../images/internal_memory_blockdiagram4.png
      :name: internal_memory_blockdiagram_img4

      Fig.1 Single cache system

Multi-core
----------
   .. figure:: ../../images/internal_memory_blockdiagram3.png
      :name: internal_memory_blockdiagram_img3

      Fig.2 Multi-core cache system
