=============
L2 Cache
=============

Overview
========

The L2 cache is a 256KB, 4-bank, 4-way set associative shared L2 cache.
The latency of L2 cache is 4 cycles at hit. The L2 cache RAM reading
and writing processes are pipelined into 4 stages for less RAM access
and higher frequency. The L2 cache is designed as a non-blocking cache
which can handle hit-under-miss and miss-under-miss using the Missing
Status Holding Registers (MSHRs). With non-blocking L2 cache design,
memory system can execute out-of-order and more latency can be hidden.

   .. figure:: ../../images/l2_1.png
      :name: Fig_1

      Fig.1 L2 cache bank connection

Parameter
---------

The parameter of L1 data cache is as follows:

  ======================   ==================   ===================   =====================
  Cache capacity           Cache line numbers   Cache line capacity   Mapping method
  ======================   ==================   ===================   =====================
  256 KBytes               512                  32 Bytes              4-way set associative
  ======================   ==================   ===================   =====================

L2 cache pipeline
=================

The L2 is designed as 4-stage-pipeline for low power and high frequency.
In the first 3 stages, valid, tag, lru, dirty and data RAMs are serially checked,
which means some of the RAMs are not needed to be accessed if the information got
from previous stages tells the control logic not to.

The Missing Status Holding Registers lie in the stage 4, which has the ability to
hold multiple cache missed request to the next level memory, without blocking the
whole pipeline. This is a key feature for Out-of-Order memory system.

   .. figure:: ../../images/l2_2.png
      :name: Fig_2

      Fig.2 L2 cache pipeline overview
