Hardware Projects
==================

This section describes the specification of PicoRio hardware components.
We have grouped all the components into 4 general classes according to their respective functionalities.
The development status is also listed.


.. toctree::
   :hidden:
   :maxdepth: 2

   ./hardware/board
   ./hardware/rrv64
   ./hardware/cache
   ./hardware/CONTRIBUTING



+------------------------------+----------------------------------------------------------+
| Component                    | Description                                              |
+==============================+==========================================================+
| `Pygmy_ES1Y Board`_          | Pygmy_ES1Y EVB User Guide                                |
+------------------------------+----------------------------------------------------------+
| `RRV64 Core`_                | RRV64 core used in PicoRio: a 64-bit, single in-order    |
|                              | issue, 5-stage-pipeline 64-bit RISC-V core.              |
+------------------------------+----------------------------------------------------------+
|  Graphics                    | Collection of display pipeline in PicoRio™.              |
|                              | This includes the GPU, display core, and video encoder   |
|                              | and decoder.                                             |
+------------------------------+----------------------------------------------------------+
| `Cache System`_              | Private L1 instruction & data cache and unified L2       |
|                              | cache.                                                   |
+------------------------------+----------------------------------------------------------+
|  System Control              | System control related features and units                |
+------------------------------+----------------------------------------------------------+
|  IO                          | Collection of input and output interfaces                |
|                              | in PicoRio hardware.                                     |
+------------------------------+----------------------------------------------------------+


.. _Pygmy_ES1Y Board: ./hardware/board.html

.. _RRV64 Core: ./hardware/rrv64.html

.. _Cache System: ./hardware/cache.html


The overall PicoRio™ hardware blockdiagram (future work included):

   .. figure:: ./images/hwstack_blockdiagram_stage3.png
      :name: Fig_1
