========
ES1Y API
========

OS API
======

The official FreeRTOS API references can be found here: https://www.freertos.org/a00106.html

UART API
========

Only support module init and printf functions for now, more functions is under developing.

::

    /********************************
                    uart
    *********************************/
    /*!
     * @discussion initialize uart module.
    */
    void __rvHal_uart_init(void);

    /*!
     * @discussion print log through uart.
     * @param fmt fmt string.
     * @param ... params corresponding to % in fmt string.
    * this is a  simplified version printf of standard printf in libc,
    * only support below format params:
    * %d, %u, %ld, %lu, %lld, %llu, %o, %x, %lo, %lx, %llo, %llx, %s, %c, %%
    * and also support width and padding in params above
     */
    int printf(const char* fmt, ...);

GPIO API
========

GPIO API is in the following code snippet 

::

    
    /********************************
                    gpio
    *********************************/

    struct irq_gpio_handler_t
    {
        void *context;
        void (*hook)(void *context);
    };

    struct gpio_desc
    {
        unsigned int pin;
        struct irq_gpio_handler_t handler;
    };

    enum RVHAL_gpio_type
    {
        GPIO_PIN_INPUT = 0,
        GPIO_PIN_OUTPUT,
    };

    enum RVHAL_gpio_int_type
    {
        GPIO_INT_TYPE_LEVEL = 0,
        GPIO_INT_TYPE_EDGE,
    };
     
    enum RVHAL_gpio_int_polarity
    {
        GPIO_INT_POLARITY_LOW = 0,
        GPIO_INT_POLARITY_HIGH,
    };

    /*!
     * @discussion initialize gpio module.
    */
    void __rvHal_gpio_init(void);

    /*!
     * @discussion initialize gpio pin descriptior.
     * @param dgpio gpio descriptor.
     * @param pin pin number[0, 31].
     * @param type see enum RVHAL_gpio_type.
     * @param value if type is GPIO_PIN_OUTPUT, it is [0, 1] by default.
     */
    void rvHal_gpio_init( struct gpio_desc *dgpio, unsigned int pin, unsigned int type, unsigned int value );

    /*!
     * @discussion set gpio pin interrupt attribution.
     * @param dgpio gpio descriptor.
     * @param level see enum RVHAL_gpio_int_type.
     * @param polarity see enum RVHAL_gpio_int_polarity.
     * @param irqHandler gpio pin callback handler.
     * @param context context param for this gpio pin.
     */
    void rvHal_gpio_set_interrupt( struct gpio_desc *dgpio, unsigned int level, unsigned int polarity, void (*irqHandler)(void*), void *context);

    /*!
     * @discussion remove gpio pin interrupt attribution.
     * @param dgpio gpio descriptor.
     */
    void rvHal_gpio_remove_interrupt( struct gpio_desc *dgpio );

    /*!
     * @discussion gpio pin output level.
     * @param dgpio gpio descriptor.
     * @param value [0, 1].
     */
    void rvHal_gpio_write( struct gpio_desc *dgpio, unsigned int value );

    /*!
     * @discussion gpio pin input level.
     * @param dgpio gpio descriptor.
     * @return value [0, 1].
     */
    unsigned int rvHal_gpio_read( struct gpio_desc *dgpio );

    /*!
     * @discussion toggle gpio pin output level.
     * @param dgpio gpio descriptor.
     */
    void rvHal_gpio_toggle( struct gpio_desc *dgpio );